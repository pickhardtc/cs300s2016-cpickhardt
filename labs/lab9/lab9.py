#Claire Pickhardt
#Computer Science / Biology 300
#Lab 9
#Due: 22 April 2016
#Honor Code: The work that I am submitting is the result of my own thinking and efforts.

from Bio.PDB import PDBParser

parser = PDBParser()

structure = parser.get_structure('1kjf', '1kjf.pdb')
output = open('output.txt', 'w')

#header = parser.get_header()
#trailer = parser.get_trailer()
#print trailer

# extract specific information from the structure
model = structure[0]
chain = model['A']
residue = chain[(' ',1,' ')]
atom1 = residue['CA']
atom2 = residue['N']

#print chain.id
#print residue.id[1], residue.resname
#print atom.name, atom.occupancy, atom.coord

#residue_list = chain.child_list
#for residue in residue_list:
 #   print residue.resname

#for residue in chain:
 #   print residue.resname

distance= []
for atom1 in structure.get_atoms():
	#(atom1-atom2)
	distance.append(atom2-atom1)
	

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plot
plot.hist(distance, len(distance)/10, facecolor = 'r')
plot.xlabel('distribution')
plot.ylabel('length')
plot.title('Distribution of Calculated Distances')
plot.grid(True)
plot.savefig("graph.pdf")
plot.cla()

