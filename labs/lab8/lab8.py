#Claire Pickhardt and Allie Landowski
#Computer Science / Biology 300
#15 April 2016
#Professors Webb and Jumadinova
#Honor Code: The work that we are submitting is the result of our own thinking and efforts.

from Bio import SeqIO

my_file = open('1q25.txt')

for record in SeqIO.parse(my_file,'fasta'):
    seq = record.seq

lenSeq = len(seq)
window = int(raw_input("Enter window size please: "))
window1= (window-1)
w = (lenSeq-window+1)
print w
ratios = [0.0] * w #(holds CpG ratio of each window)
for i in range(0,w):
    cCtr = 0.0
    gCtr = 0.0
    cgCtr = 0.0
    for j in range (0,window1):
        if seq[j+i] == 'C':
            cCtr+=1
            if seq[i+j+1] == 'G':
                cgCtr+=1
        elif seq[j+i] == 'G':
            gCtr+=1
    if cCtr*gCtr != 0:
        ratios[i] == cgCtr/((cCtr*gCtr)/window)
    else:
        ratios[i] == 0
for i in range (0,len(ratios)):
    if ratios[i] > 1.5:
        print i+1, ratios[i], '***'
    else:
        print i+1, ratios[i]

