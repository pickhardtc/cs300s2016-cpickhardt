#Claire Pickhardt
#Computer Science 300
#3 March 2016
#Honor Code: The work that I am submitting is the result of my own thinking and efforts.
#Citing: Keegan and Ted
import difflib
from string import maketrans


codons = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
"UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
"UAU":"Y", "UAC":"Y", "UAA":"*", "UAG":"*",
"UGU":"C", "UGC":"C", "UGA":"*", "UGG":"W",
"CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
"CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
"CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
"CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
"AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
"ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
"AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
"AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
"GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
"GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
"GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
"GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}

fastaONE = open('MutantAWildType.txt')
fastaTWO = open('MutantB.txt')


fastaONEn = fastaONE.readline()
fastaTWOn = fastaTWO.readline()


fastaONE = ''.join(fastaONE.readlines())
fastaTWO = ''.join(fastaTWO.readlines())

fastaONE = fastaONE.replace('\n','')
fastaTWO = fastaTWO.replace('\n','')

fastaONE = fastaONE.translate(maketrans('ATGC', 'UACG'))
fastaTWO = fastaTWO.translate(maketrans('ATGC', 'UACG'))

trans1 = ''
trans2 = ''
for i in xrange(0, len(fastaONE), 3):
    trans1 += codons[fastaONE[i:i+3]]
    trans2 += codons[fastaTWO[i:i+3]]

trans1 = [trans1 + '\n']
trans2 = [trans2 + '\n']


delta = difflib.ndiff(trans1, trans2)
print ''.join(delta)

with open('output_short.html','w') as o:
    delta = difflib.HtmlDiff().make_file(trans1, trans2, fromfile=fastaONEn, tofile=fastaTWOn)
    o.writelines(delta)


