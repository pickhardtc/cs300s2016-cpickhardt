#Claire Pickhardt
#Computer Science 300
#3 March 2016
#Honor Code: The work that I am submitting is the result of my own thinking and efforts.
#Keegan Shudy and Ted Avolio helped me with portions of this lab because they were present during my office hours.


from string import maketrans

FAIL = '\033[91m'
ENDC = '\033[0m'

def main():
    codons = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
    "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":"*", "UAG":"*",
    "UGU":"C", "UGC":"C", "UGA":"*", "UGG":"W",
    "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
    "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
    "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
    "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
    "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
    "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
    "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
    "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}

    fastaONE = open('codingSeq.fasta')
    fastaTWO = open('codingSeq_(copy).fasta')

    fastaONEn = fastaONE.readline()
    fastaTWOn = fastaTWO.readline()
    fastaONE = ''.join(fastaONE)
    fastaTWO = ''.join(fastaTWO)
    fastaONE = fastaONE.replace('\n','').upper()
    fastaTWO = fastaTWO.replace('\n','').upper()
    fastaONEtransc = fastaONE.translate(maketrans('ACTG', 'UGAC'))
    fastaTWOtransc = fastaTWO.translate(maketrans('ACTG', 'UGAC'))
    fastaONEtransl = ''
    fastaTWOtransl = ''
    mutation_lines = []

    for i in xrange(0, len(fastaONEtransc), 3): #for statement for transcribing fastaONE
        if fastaONEtransc[i:i+3] in codons.keys(): #nested if for keying in codons
            fastaONEtransl += codons[fastaONEtransc[i:i+3]]
        else: #else statement
            fastaONEtransl += '-'

    for i in xrange(0, len(fastaTWOtransc), 3): #for statement for transcribing fastaTWO
        if fastaONEtransc[i:i+3] in codons.keys():#nested if for keying in codons
            fastaONEtransl += codons[fastaTWOtransc[i:i+3]]
        else: #else statement
            fastaTWOtransl += '-'

    with open('diff.html', 'w') as output:
        print fastaONEn,
        output.writelines('<h3> %s </h3>' % (fastaONEn))


        if len(fastaONEtransl) < len(fastaTWOtransl):
            for i in xrange(0, len(fastaONEtransl)):
                if fastaONEtransl[i] != fastaTWOtransl[i]:
                    print (FAIL + fastaONEtransl[i] + ENDC),
                    output.write('%s' % (fastaONEtransl[i]))
                    mutation_lines.append(i)
                else:
                    print (fastaONEtransl[i]),
                    output.write(' ' + fastaONEtransl[i])

            print '\n' + fastaTWOn,
            output.writelines('<br /> <br /><h3 %s </h3>' % (fastaTWOn))

            for i in xrange(0, len(fastaONEtransl)):
                if fastaTWOtransl[i] != fastaONEtransl[i]:
                    print (FAIL + fastaTWOtransl[i]+ ENDC),
                    output.write('%s' % (fastaTWOtransl[i]))
                else:
                    print (fastaTWOtransl[i]),
                    output.write(' ' + fastaTWOtransl[i])

        else:
            for i in xrange(0, len(fastaTWOtransl)):
                if fastaONEtransl[i] != fastaTWOtransl[i]:
                    output.write('%s' % (fastaONEtransl[i]))
                    mutation_lines.append(i)
                    print (FAIL + fastaONEtransl[i] + ENDC),

                else:
                    print (fastaONEtransl[i]),
                    output.write(' ' + fastaONEtransl[i])

            print '\n\n' + fastaTWOn,
            output.writelines('<br /> <br /> <h3> %s </h3>' % (fastaTWOn))

            for i in xrange(0, len(fastaTWOtransl)):
                if fastaTWOtransl[i] != fastaTWOtransl[i]:
                    print (FAIL + fastaTWOtransl[i] + ENDC),
                    output.write('%s' % (fastaTWOtransl[i]))
                else:
                    print (fastaTWOtransl[i]),
                    output.write(' ' + fastaONEtransl[i])

        print '\n Mutations exist in these lines: ', mutation_lines
        output.writelines('<br /> <h3>Mutation at lines: %s <h3>' % (mutation_lines))
    output.close()



if __name__ == '__main__':main()
