#Claire Pickhardt
#Computer Science / Biology 300
#1 April 2016 (sorry this is so late!)
#Lab 7
#Honor Code: The work that I am submitting is the result of my own thinking and efforts.

from Bio import Entrez
from Bio import SeqIO

my_file = Entrez.read(Entrez.esearch(db='protein', term='12513076', retmode='xml'))
my_list = my_file["IDList"]

handle = Entrez.efetch(db="protein", rettype="gb", retmode="text", id=', '.join(my_list))

output = open("yagW.txt", "w")

for seq_record in SeqIO.parse(handle, "gb"):
    print("%s %s>>>" % (seq_record.id, seq_record.description[:50]))
    print("Sequence length %i, %i features, from: %s"
          % (len(seq_record), len(seq_record.features), seq_record.annotations["source"]))
    SeqIO.write(seq_record, output, "fasta")

output.close()
handle.close()
