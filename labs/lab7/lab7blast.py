#Claire Pickhardt
#1 April 2016
#lab 7
#computer science / biology 300
#honor code: the work that i am submitting is the result of my own thinking and efforts.
#any help i received was from nate gervais because i was not present during the lab session.

from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
from Bio import SeqIO

record = SeqIO.read("yagW.fasta", formal="fasta")
handle = NCBIWWW.qblast("blastp", "nr", record.seq, alignments=50, descriptions=50)
new_file = open("yagW_blast.xml", "w")
new_file.write(handle.read())
handle.close()
handle = open("yagW_blast.xml")
blast_recs = NCBIXML.parse(handle)
blast_rec = blast_recs.next()
output = open("yagW.txt", "w")

value_threshold = 0.08

for alignment in blast_rec.alignments:
    for hsp in alignment.hsps:
        if hsp.expect < value_threshold:
            output.write(">>>>>Alignment<<<<<<<<<")
            output.write(alignment.title)
            length = len(alignment)
            output.write(str(length))
            expect = hsp.expect
            output.write(str(expect))
            output.write(hsp.query[0:75]+"...")
            output.write(hsp.match[0:75]+"...")
            output.write(hsp.sbjct[0:75]+"...")
output.close()

